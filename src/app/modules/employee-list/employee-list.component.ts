import { EmployeeDetailsComponent } from './../employee-details/employee-details.component';
import { Observable } from "rxjs";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/class/employee';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: "app-employee-list",
  templateUrl: "./employee-list.component.html",
  styleUrls: ["./employee-list.component.css"]
})
export class EmployeeListComponent implements OnInit {

  dataSource: MatTableDataSource<Employee>
  employees: Employee[];
  columns: String[] = ['id','firstName','lastName','emailId','address','action'];

  @ViewChild(MatSort,{static:true}) sort: MatSort;
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;

  constructor(private employeeService: EmployeeService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData().subscribe((result)=>{    
      this.employees  =  result;
      this.dataSource = new MatTableDataSource(this.employees);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator; 
    });
    
  }

  reloadData() {
    return this.employeeService.getEmployeesList();
  }

  applyFilter(event){
    const serachValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = serachValue.trim().toLowerCase();
  }

  deleteEmployee(id: number) {

    var confirm = window.confirm("Are you sure, want to delete ?");

    if(confirm){
    this.employeeService.deleteEmployee(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
          this.gotoList();
        },
        error => console.log(error));
      }
      else{
          this.reloadData();
         }
  }

  employeeDetails(id: number){
    this.router.navigate(['details', id]);
  }

  updateEmployee(id: number){
    console.log(id);
    this.router.navigate(['update', id]);
  }

  gotoList() {
    this.router.navigate(['/employees']);
  }
}
