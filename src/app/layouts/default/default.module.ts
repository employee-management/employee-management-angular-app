import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSidenavModule, MatDividerModule, MatCardModule, MatPaginatorModule, MatTableModule, MatSortModule, MatInputModule, MatFormFieldModule, MatIconModule, MatButton, MatButtonModule, MatMenuModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CreateEmployeeComponent } from 'src/app/modules/create-employee/create-employee.component';
import { EmployeeDetailsComponent } from 'src/app/modules/employee-details/employee-details.component';
import { EmployeeListComponent } from 'src/app/modules/employee-list/employee-list.component';
import { UpdateEmployeeComponent } from 'src/app/modules/update-employee/update-employee.component';
import { EmployeeService } from 'src/app/service/employee.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    DefaultComponent,
    CreateEmployeeComponent,
    EmployeeDetailsComponent,
    EmployeeListComponent,
    UpdateEmployeeComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [
    EmployeeService
  ]
})
export class DefaultModule { }
