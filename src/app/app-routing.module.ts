import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { CreateEmployeeComponent } from './modules/create-employee/create-employee.component';
import { EmployeeDetailsComponent } from './modules/employee-details/employee-details.component';
import { EmployeeListComponent } from './modules/employee-list/employee-list.component';
import { UpdateEmployeeComponent } from './modules/update-employee/update-employee.component';

const routes: Routes = [{
  path: '',
  component: DefaultComponent,
  children: [ 
  {
    path: 'employees',
    component: EmployeeListComponent
  },
  {
    path: 'addEmployee',
    component: CreateEmployeeComponent
  },
  { path: 'update/:id',
   component: UpdateEmployeeComponent 
  },
  { path: 'details/:id', 
  component: EmployeeDetailsComponent  
  } 
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
